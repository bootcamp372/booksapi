﻿using BooksAPI.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using static System.Net.Mime.MediaTypeNames;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System;
using System.Reflection.PortableExecutable;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using Microsoft.EntityFrameworkCore;

namespace BooksAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : Controller
    {
/*        private readonly List<Book> _books =
            new List<Book>
            {
            new Book { BookId = 1,
            Title = "Cat's Cradle",
            Author = "Kurt Vonnegut",
            Publisher = "Penguin",
            YearPublished = 1957,
            Price = 13.99M}
            ,
            new Book { BookId = 2,
            Title = "Hitchhiker's Guide to the Galaxy",
            Author = "Douglas Adams",
            Publisher = "Random House",
            YearPublished = 1983,
            Price = 15.99M}
            };*/
        // GET api/books (return all books)
        [HttpGet]
        public ActionResult<Book> Get()
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = BooksDB; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Book> _books = new List<Book>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Books";
            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Book book = new Book();
                book.BookId = (int)reader[0];
                book.Title = (string)reader[1];
                book.Author = (string)reader[2];
                book.Publisher = (string)reader[3];
                book.YearPublished = (int)reader[4];
                book.Price = (decimal)reader[5];
                _books.Add(book);
            }
            reader.Close();
            conn.Close();
            return Ok(_books);
        }
        // GET api/books/1 (return book where id=1)
        [HttpGet("{id}")]
        public ActionResult<Book> Get(int id)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = BooksDB; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Book> _books = new List<Book>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "SELECT * FROM Books WHERE BookId = @BookId";
            
            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.Parameters.AddWithValue("@BookId", id);
            SqlDataReader reader = cmd.ExecuteReader();
            Book book = new Book();
            while (reader.Read())
            {
                
                book.BookId = (int)reader[0];
                book.Title = (string)reader[1];
                book.Author = (string)reader[2];
                book.Publisher = (string)reader[3];
                book.YearPublished = (int)reader[4];
                book.Price = (decimal)reader[5];
            }

            reader.Close();
            conn.Close();
            return Ok(book);
        }

        // POST api/books
        [HttpPost]
        public ActionResult Post(Book book)
        {
            string connStr = "Data Source = (localdb)\\MSSQLLocalDB; Initial Catalog = BooksDB; Integrated Security = True; Connect Timeout = 30; Encrypt = False; Trust Server Certificate = False; Application Intent = ReadWrite; Multi Subnet Failover = False";
            List<Book> _books = new List<Book>();
            SqlConnection conn = new SqlConnection(connStr);
            conn.Open();
            string query = "INSERT INTO Books(Title, Author, Publisher, YearPublished, Price) VALUES('" + book.Title + "','" + book.Author + "','" + book.Publisher + "'," + book.YearPublished + "," + book.Price + ")";

            SqlCommand cmd = new SqlCommand(query, conn);

            int rowsChanged = cmd.ExecuteNonQuery();
            Console.WriteLine($"{rowsChanged} rows changed");

            conn.Close();


            if (book.YearPublished > DateTime.Today.Year)
            {
                return BadRequest("Invalid data"); // Status 400
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _books.Add(book);
            string location = $"api/Books/{book.BookId}";
            return Created(location, book);
        }

        [HttpPut("{id}")]
        public ActionResult Put(Book book, int id)
        {
            var dbContext = new BooksDbContext();
            string location = "";
            Book bookUpdated = dbContext.Books.FirstOrDefault(b => b.BookId == id);

            if (bookUpdated == null)
            {
                dbContext.Add(book);
                
                dbContext.SaveChanges();
                location = $"api/Books/{book.BookId}";
                return Created(location, book);
            }
            else {
                // execute put
                bookUpdated.Author = book.Author;
                bookUpdated.Title = book.Title;
                bookUpdated.YearPublished = book.YearPublished;
                bookUpdated.Publisher = book.Publisher;
                bookUpdated.Price = book.Price;
                dbContext.SaveChanges();
                return Ok();
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var dbContext = new BooksDbContext();
            string location = "";
            Book bookDeleted = dbContext.Books.FirstOrDefault(b => b.BookId == id);

            Console.WriteLine(id);

            if (bookDeleted == null)
            {
                return NotFound();
            }
            else
            {
                dbContext.Remove(bookDeleted);
                dbContext.SaveChanges();
                return Ok();
            }
        }

    }
}



